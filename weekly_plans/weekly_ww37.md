---
Week: 37
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 37 ITT3 IoT

## Goals of the week(s)
IoT systems overview. Before building an IoT solution examing the enviroment. We will be looking into an existing IoT project that is cross country.
We will use external material to look into a complicated system with respect to build and security. 

### Practical goals
1. See an IoT case from a real project.
2. Review article 
3. Generate documentation and study case.

### Learning goals
1. Hand-in Project and Case

## Deliverable

1. Students will be exposed to IoT and security as well as build of ecosystem


## Schedule

See Time Edit

## Hands-on time

N/A

## Comments
N/A
