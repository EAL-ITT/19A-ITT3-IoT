---
Week: 40
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 40 ITT3 IoT

## Goals of the week(s)
Presentation of a IoT protocols. The lecture will present a article with focus on smart grid. Article is titled:Applying ZigBee wireless sensor and control network for bridge safety monitoring

### Practical goals
1. Develop a case based on the article.
2. Look for the challenges of the project.
3. Risk Assessment.

### Learning goals
1. IoT technologies and implementation in security.

## Deliverable
N/A

## Schedule

See Time Edit

## Hands-on time

N/A

## Comments
N/A
