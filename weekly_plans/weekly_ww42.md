---
Week: 42+
Content:  Future work with RUTR
Material: N/A
Initials: RUTR
---

# Week 42+ ITT3 Free time activity and project

## Goals of the week(s)
Self Reading White paper on ARM Cortex M for beginners.

### Practical goals
1. Read the white paper

### Learning goals
1. Learn a bit about ARM
2. Developed a simple LED blink if you want to.

## Deliverable
N/A

## Schedule

See Time Edit

## Hands-on time

N/A

## Comments
This is not mandatory work.
