---
Week: 38
Content:  This weeks subjects
Material: See links in weekly plan
Initials: RUTR
---

# Week 38 ITT3 IoT

## Goals of the week(s)
Presentation of a IoT protocols. The lecture will present a article with focus on smart grid. Article is titled: ZigBee Tecnology Application in Wireless Communication Mesh Network of Ice Disaster

### Practical goals
1. The technology selection phase, the class will find the wireless technology that will be used for the mini projet.

### Learning goals
1. IoT technologies and implementation in security.

## Deliverable
N/A

## Schedule

See Time Edit

## Hands-on time

N/A

## Comments
N/A
