![Build Status](https://gitlab.com/EAL-ITT/19A-ITT3-IoT/badges/master/pipeline.svg)


# 19A-ITT3-IoT

weekly plans, resources and other relevant stuff for the 3. semester IoT module in IT technology 3. semester autumn of 2019 class.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/19A-ITT3-IoT/)
