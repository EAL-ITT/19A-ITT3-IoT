---
title: '19A-ITT3-IoT'
subtitle: 'Lecture plan'
authors: ['Ruslan Trifonov \<rutr@ucl.dk\>']
main_author: 'Ruslan Trifonov'
date: \today
email: 'rutr@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or IoT.

* Study program, class and semester: IT technology, oeait18, 19A
* Name of lecturer and date of filling in form: RUTR, 2019-09-08
* Title of the course, module or Class and ECTS: IoT, 5 ECTS
* Required readings, literature or technical instructions and other background material: RPI, various sensors

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------
RUTR
---------------------------------------------------------


# General info about the course
The Course will be focusing on practical skill, examination of papers and system development. Focus will be Risk Analysis, Planing and Modeling of Systems. The students will be presenting their own solution on a given problem based on the article and technology presented in the class. Each article will touch a point that IoT can improve a situation or IoT is missing and students are tasked with creating a solution from design point of view.

## The student’s learning outcome

### Knowledge

Students will understand the planning of a IoT system. The challenges that it will represent to the set up one, as well as the fundamental theoretical knowledge.

### Skills

Students will develop skills in the IoT sector, as well as various techniques while looking into use of existing technologies and refocusing them to IoT.

### Competencies

Practical understanding of planing, and risk as well as security challenged, environmental or social challenges in IoT.

## Content

The course will be split into theoretical part that will give a base of knowledge and then focus on the practical part, with focus into understanding IoT

## Method

The course will have approximately 20 present of the time theory and the rest will be work on the assigned tasks.

## Equipment

Ability to go online, create documentation and presentations.

## Course Project with external collaborators  

The course will be with no external collaborators.

## Test form/assessment
The assessment form is a presentation, with a pass/fall out come. No grade is given. The successful student can answer all the questions in the following topics.


### IoT and security
The student can demonstrate knowledge around IoT and security. The consideration around security issues and possible solutions.

### IoT technologies
The students has to demonstrate basic knowledge and understand IoT as a demonstrator of solution, or assistance, as well as quality of life and services.

## Other general information
The students are welcomed to continue working with zigbee or other IoT technology in the future.
